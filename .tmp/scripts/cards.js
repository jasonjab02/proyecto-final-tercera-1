console.log('cargando cards...');
const dataCards = [{
  "title": "EXAMEN CINTURÓN BLANCO",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}, {
  "title": "EXAMEN CINTURÓN AMARILLO",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}, {
  "title": "EXAMEN CINTURÓN VERDE",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}, {
  "title": "EXAMEN CINTURÓN AZUL",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}, {
  "title": "EXAMEN CINTURÓN ROJO",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}, {
  "title": "EXAMEN CINTURÓN ROJO II",
  "url_Image": "https://static.vecteezy.com/system/resources/previews/004/903/282/non_2x/cartoon-character-of-taekwondo-girl-vector.jpg",
  "desc": "Examen escrito tkd Jansu 2022",
  "cta": "EMPEZAR...",
  "link": "ExamenB.html"
}];

(function () {
  let CARD = {
    init: function () {
      console.log('El modulo carga correctamente');

      let _self = this; //Lllamamos a las funciones


      this.insertData(_self);
    },
    eventHandler: function (_self) {
      let arrayRefs = document.querySelectorAll('.card-title');

      for (let x = 0; x < arrayRefs.length; x++) {
        arrayRefs[x].addEventListener('Click', function (event) {
          console.log('Evento', event);

          _self.showTab(event.Target);
        });
      }
    },
    insertData: function (_self) {
      dataCards.map(function (item, index) {
        document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tlpCardItem(item, index));
      });
    },
    tlpCardItem: function (item, index) {
      return `<div class='card-item' id="card_number-${index}">
            <img src="${item.url_Image}"/>
            <div class="card-info">
                <p class='card-tittle'>${item.title}</p>
                <p class='card-desc'>${item.desc}</p>
                <a class='card-cta' target='blank' href="${item.link}">${item.cta}</a>
                
            </div>
            </div>`;
    }
  };
  CARD.init();
})();
//# sourceMappingURL=cards.js.map

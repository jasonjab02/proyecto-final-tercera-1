console.log('cargando Accordion...');
    const dataAccordion = [
        {
          "tittle": "Historia de Taekwondo  de Costa Rica",
          "desc": "El Taekwondo fue introducido a Costa Rica por el Maestro Aquiles Won Kun Yan Yu en el año 1969. En ese mismo año se practica por primera vez en la Universidad de Costa Rica. La primera academia que se fundo fue el Instituto de Taekwondo de Costa Rica y estuvo situado en el Barrio Amón en San José, 200 metros oeste del Instituto Nacional de Seguros. El primer cinturón negro graduado en Costa Rica y oficialmente reconocido fuel el Sr. Alejandro Chacón Zumbado. Existe otro cinturón negro llamado Jaime Córdoba, pero este practicó Karate antes de Taekwondo. El campeón Mundial de Rompimiento en salto alto en un evento no oficializado por la WTF, fue el Costarricense Guillermo Hope Marín quién era alumno del maestro Aquiles Won Kun Yan Yu y del Instituto de Taekwondo de Costa Rica. El fundador de la Asociación Costarricense de Taekwondo fue el Prof. Nelson Brizuela Cortéz en el año 1982 y en 1983 obtiene el reconocimiento de la Federación Mundial de Taekwondo. El profesor Brizuela fue presidente de la Asociación en los periodos del 1983 a 1987. Además es uno de los referis Internacionales en Latinoamérica que obstenta el Grado de Clase A. El primer presidente de La Asociación Costarricense de Taekwondo fue el Arquitecto Roberto Pauly Laspiur quien fuera un excelente competidor a nivel Internacional. Del 30 de septiembre al 02 de octubre de 1994 se efectuó en Costa Rica el IX Campeonato Panamericano de Taekwondo. Siendo Presidente de la Asociación el Lic. Sergio Masis Olivas. ",  
        },
        {
            "tittle": "Historia Antigua de Taekwondo",
            "desc": "Los antiguos habitantes de lo que hoy es Corea, se organizaban en tribus, de las cuales existen indicios, como pinturas en techos de cuevas o murales en tumbas de personalidades, de la practica de artes marciales. Comenzaron como danzas, ritos religiosos y ejercicios para mejorar la salud y luego se incorporaron a la defensa contra tribus rivales, especialmente movimientos como posturas y actitudes defensivas y ofensivas copiadas de animales tales como tigres y osos. Así llegamos a unos pocos años antes de Cristo. En el 57 A.C. y 37 A.C. se fundaron dos reinos que constituyen prácticamente el territorio de lo que es hoy Corea del Sur Silla y Koguryo. También por esos tiempos se fundo el tercer reino Paetché, que fue casi igual en dimensión a lo que hoy es Corea del Norte. En todos ellos hay indicios de la práctica de artes marciales rudimentarias, pero parcialmente en Silla, se especializaron en ellas unos guerreros, los Hwarang-Do, famosos por su disciplina y su ferocidad. Estos se consideraban dioses y para lograrlo debían reunir como ellos lo femenino y lo masculino. Por eso se vestían y pintaban como mujeres. Además bailaban y danzaban al pie de las montañas y orillas de los ríos porque los consideraban divinos y querían tomar fuerza de ellos. Así los tres reinos quedaron unificados al ser conquistados Koguryo y Paetché por Silla gracias a los guerreros mencionados, en el año 427 D.C. los Hwarang-Do cayeron en desgracia con la llegada del Budismo y sus influencias con preceptos como el de no matar. De las muchas artes marciales que se practicaban en la antigua Corea, cabe destacar el Soobak-Do. Más adelante fue el Tae Kyon y el más reciente antes del Taekwondo fue el Tang Su Do." , 
        },
        {
            "tittle": "Historia de Taekwondo Moderno",
            "desc": "En 1910 Japón invade Corea sin resistencia de esta última por conocer el fabuloso poderío nipón. Los invasores prohíben la práctica de toda manifestación cultural y deportiva no japonesa y sólo permiten a los policías coreanos que practiquen el Karate (japonés) como arte marcial para que colaboren en el mantenimiento del orden. Durante la ocupación, sólo algunos coreanos en el exilio (China, por ejemplo) siguieron practicando sus artes nacionales coreanas, mientras que en los territorios ocupados sólo lo hacían a escondidas y especialmente en las familias, donde los padres trasmitían sus conocimientos a los hijos. En 1945, con el fin de la 2º guerra mundial, también terminó el dominio japonés. Volvieron algunos coreanos que vivían fuera del país, y junto con los que tenían algunos conocimientos marciales, reiniciaron la práctica del Tae Kyon, Tang Su Do y otras artes menores, aunque los 35 años de dominio japonés habían dejado sus huellas, y la influencia del Karate se notó durante muchos años. Poco a poco se fueron diferenciando cada vez más los movimientos fuertes, duros y trabados del estilo japonés de los movimientos veloces y ágiles del estilo coreano. Al ordenársele la reestructuración de uno de los cuerpos militares del país, se dedicó a la búsqueda de maestros de artes marciales para adiestrar a los soldados en la mejor manera de combate cuerpo a cuerpo. Así, trató de combinar lo mejor de distintas artes, incluso el Karate que él mismo practicaba, llegando diez años después del fin de la guerra, en 1955, a crear un arte denominado TAEKWONDO. Tae= pie; Kwon= puño; Do= camino, es decir el camino de los pies y de los puños. Convertido en General, su acercamiento a Corea del Norte no fue bien visto por los militares de Corea del Sur, acusándolo de comunista y de intentar hacer una revolución a favor de esta última. Así fue que en su carácter de Presidente de la Asociación Coreana de Taekwondo fundó en 1966 la I.T.F. (International Taekwondo Federation) y debió irse a Canadá para evitar ser condenado por subversivo. Las autoridades coreanas sosteniendo que este arte era propiedad de todo un país y no de una sola persona u organización, crearon en 1970 la W.T.F. (World Taekwondo Federation o Federación Mundial de Taekwondo) con sede en Seúl. En Corea este es el único estilo oficial que se sigue practicando, prohibiendo por medio de sus embajadas en todo el mundo que siguieran en la I.T.F. Las presiones diplomáticas hicieron que solo unos pocos seguidores del general y sus más antiguos discípulos permanecieran en la I.T.F., con la prohibición de volver a Corea. Los cambios: El estilo I.T.F., influenciado por el Full Contact americano, adoptó protectores de pies y manos y cambió el estilo de combate prohibiendo el contacto pleno y haciendo las luchas sólo a marcar, además de mantener las formas tradicionales. El estilo W.T.F. siguió con su sistema de lucha, pero al protector pectoral tradicional se le fue agregando otras protecciones: cabezal, inguinal, canilleras, protecciones de antebrazos, y luego fueron el bucal y los guantes, cambiando las formas por considerar que las primeras estaban muy influenciadas por el Karate. De todos modos, nuestra impresión es que las formas que se practican hoy también están desactualizadas en lo que respecta al Taekwondo moderno, ya que si bien algunos bloqueos siguen siendo efectivos, estos son más aplicables a una postura rígida o estática que a los veloces desplazamientos utilizados actualmente. El estilo W.T.F. es oficial en todos los países del mundo y está reconocido como deporte olímpico desde 1998, participando en los Juegos Panamericanos como deporte oficial desde 1977 (el Karate lo haría recién en 1995) El estilo I.T.F. es una entidad privada sin apoyo oficial, que luego de la muerte de Choi Hong Hee se dividió en numerosas instituciones que no se reconocen entre sí. Así es que en muchos países ya no se practica el Taekwondo I.T.F. y en muchos otros se hace en escala muy reducida. En 1980 de declara al Taekwondo WTF deporte olímpico, participando como deporte demostración en los Juegos Olímpicos de Seúl 1988 y Barcelona 1992. Los Juegos olímpicos se denominan por cuestiones políticas por el nombre de la ciudad y no del país)  En 1994, el Comité Olímpico Internacional lo elevó al rango de deporte oficial, o sea que las medallas obtenidas a partir de allí serían consideradas en el medallero, cosa que no ocurre con los deportes demostración. En Atlanta 1996 no participó, ya que el Comité olímpico establece un período de carencia después de ser nombrado deporte oficial, en el cual no participa en los Juegos para que todos los países del mundo que no tengan incorporado este deporte puedan tener tiempo para desarrollarlo y tener atletas en condiciones de obtener medallas.Recién en Sydney, en el año 2000." , 
        },
    ];

    (function (){

        let Accordion = {
            init: function (){
                let _self = this
                //llamamos las funciones
                this.insertData(_self);
                this.eventHandler(_self);

            },

            insertData: function(_self){
                dataAccordion.map(function(item,index) {
                    document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend',_self.tplAccordionItem(item));
                });
            },

            tplAccordionItem: function (item) {
                return (`<div class='accordion-item'>
                <div class='accordion-tittle'><p>${item.tittle}</p></div> 
                <div class='accordion-desc'><p>${item.desc}</p></div> 
                </div>`)

            },

            eventHandler: function (_self) {
                let arrayRefs = document.querySelectorAll('.accordion-tittle');

                for (let x = 0; x < arrayRefs.length; x++) {

                    arrayRefs[x].addEventListener('click', function (event){
                        console.log('event',event);
                        _self.showTab(event.target);
                    });
                }

            },

            showTab: function (refitem) {
              let activeTab = document.querySelector('.tab-active')
              if ( activeTab){
                activeTab.classList.remove('tab-active');
              }  
              
              console.log('show tab', refitem);
              refitem.parentElement.classList.toggle('tab-active');
            },
        }
        Accordion.init()
    })();
const hamburgerIcon = document.querySelector('.hamburger-icon');
const headerMenu = document.querySelector('.header-menu');

let hamburgerIconOpen = false;

hamburgerIcon.addEventListener('click',() => {

    if(!hamburgerIconOpen){
        hamburgerIcon.classList.add('start');
        hamburgerIconOpen = true;
        headerMenu.classList.add('show');
    }else{
        hamburgerIcon.classList.remove('start');
        hamburgerIconOpen = false;
        headerMenu.classList.remove('show');
    }
    
}); 
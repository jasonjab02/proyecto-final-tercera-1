<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Academia Jansu Atenas</title>
    <link rel="shortcut icon" href="images/jansu_logo.png" type="image/x-icon">
    <link rel="stylesheet" href="/styles/style.css">
    
</head>

<body>

    <header>
      
        <div class="header">
            <div class="logo">
                <img src="/images/jansu_logo.png" alt="">
                
                     <h1 id="tittle1">TKD<br>Jansu<br>Atenas</h1>
                     <h1 id="tittle2">Taekwondo Jansu Atenas</h1>   
            </div>
            
            <div class="hamburger-icon">
                <div class="sub-hamburger-icon">

                </div>
            </div>

        </div>
        <hr>
        <div class="header-menu">
            <a href="index.html">INICIO</a><hr>
           
            <a href="horariosytarifas.html">HORARIOS Y TARIFAS</a><hr>
            <a href="ubicacion.html">VISITANOS</a><hr>
            
            <a href="contact-us.html">CONTACTENOS</a><hr>
            <a href="login.html">LOGIN</a><hr>
        </div>

    </header>
   

    <main>
        <div class="form">
            <h2>Contáctenos</h2> 
            
            <div class="form-2">
            <div class="contacto">
                <a href="tel:+50672444454"><img src="/images/icono-telefono.jpg" alt=""><P>¡Llámanos!</P></a>
                <a href="https://api.whatsapp.com/send?phone=50672444454&&text=Hola%21%20¿Me%20podrían%20brindar%20información%20de%20las%20clases%20de%20taekwondo?%20"  target="_blank"> <img src="/images/whatsapp-logo.png" alt=""><p>¡Escríbenos!</p></a>
                <a href="https://www.facebook.com/JansuCostaRica" target="_blank"><img src="/images/facebook-logo.png" alt=""><p>¡Visítanos!</p></a>
            </div>

            <form class="sub-form" method="post">
                <label for="name">Nombre Completo:</label>
                <input type="text" name="name" maxlength="40" required>
                <label for="phone">Teléfono:</label>
                <input type="tel" name="phone" placeholder="Usa únicamente números" maxlength="8" required>
                <label for="comments">Envíanos tus consultas:</label>
                <textarea name="comments" style=" resize: none" required></textarea>
                <input type="submit" name="send" value="Enviar">
            </form>
            <?php
			    include("validate.php");
    		?>
        </div>
        </div>
    </main>
    
        
    
    
    
    <footer>

        <div id="flotantes" class="flotantes">
            <a href="tel:+50672444454"><img src="/images/icono-telefono.jpg" alt=""></a>
            <a href="https://api.whatsapp.com/send?phone=50672444454&&text=Hola%21%20¿Me%20podrían%20brindar%20información%20de%20las%20clases%20de%20taekwondo?%20"  target="_blank"> <img src="/images/whatsapp-logo.png" alt=""></a>
            <a href="https://www.facebook.com/JansuCostaRica" target="_blank"><img src="/images/facebook-logo.png" alt=""></a>
        </div>
        <div class="footer-info">
            <div class="footer-info-tittle">
             
            </div>
           <div class="footer-enlaces">
            <a href="index.html">INICIO</a><hr>
            
            
            <a href="contact-us.html">CONTACTENOS</a>
            
        </div>
        <p class="derechos">Todos los derechos reservados,2022  ByGoal</p>
          </div>


        
        <script src="/scripts/main.js"></script>
        <script src="/scripts/accordion.js"></script>
    </footer>




</body>
</html>